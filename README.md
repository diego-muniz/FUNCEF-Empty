# Componente - FUNCEF-Status

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Empty Verifica se o campo está vazio no formulário.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-empty --save.
```

## Script

```html
<script src="bower_components/funcef-empty/dist/funcef-empty.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-empty/dist/funcef-empty.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-empty dentro do módulo do Sistema.

```js
angular
        .module('funcef-demo', ['funcef-empty']);
```

## Uso

```html
<dd ngf-empty></dd>
```

## Desinstalação:

```
bower uninstall funcef-empty --save
```

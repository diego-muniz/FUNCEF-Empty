﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Empty
    * @version 1.0.0
    * @Componente Empty 
    */
    angular.module('funcef-empty.controller', []);
    angular.module('funcef-empty.directive', []);

    angular
    .module('funcef-empty', [
      'funcef-empty.controller',
      'funcef-empty.directive'
    ]);
})();
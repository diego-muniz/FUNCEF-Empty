﻿(function () {
    'use strict';

    angular
      .module('funcef-empty.directive')
      .directive('ngfEmpty', ngfEmpty);

    ngfEmpty.$inject = [];

    /* @ngInject */
    function ngfEmpty() {
        return {
            restrict: 'EA',
            scope: {},
            link: function (scope, elem, attrs) {
                var defaultOptions = {
                    textEmpty: 'Não informado',
                    classEmpty: 'text-empty',
                    iconEmpty: false,
                    classIconEmpty: 'fa fa-folder-open'
                };

                var opts = angular.extend({}, defaultOptions, attrs);

                // @TODO - Adicionar apenas ao elemento ao invés do escopo inteiro
                scope.$watch(function () {
                    elem.find('div.' + opts.classEmpty).remove();
                    var text = elem[0].innerText.replace(/[\s]/g, '');
                    var html = elem.text().replace(/[\s]/g, '');

                    if (!text.length && !html.length) {
                        if (opts.iconEmpty) {
                            elem.append('<div class="list-empty ' + opts.classEmpty + '"> <i class="' + opts.classIconEmpty + '" /> <p>' + opts.textEmpty + '</p> </div>');
                        } else {
                            elem.append('<div class="' + opts.classEmpty + '">' + opts.textEmpty + '</p>');
                        }
                    }
                });
            }
        };
    }
})();